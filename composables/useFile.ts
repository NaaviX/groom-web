import { ref as storageRef, uploadBytes, getStorage, deleteObject, getDownloadURL, UploadResult } from "firebase/storage";
import { Step } from "./useStep";

export interface FileInfos {
    folder: string;
    fileName: string;
}

export interface FileToUpload {
    file: File;
    folder: string;
    object: Step;
}

export const useFile = () => {
    const { firestore } = useFirestore();
    const storage = getStorage(firestore.app)

    const onUpload = (fileToUpload: FileToUpload): FileInfos => {
        console.log("onUpload")
        const folder = fileToUpload.folder + '/'
        const fileName = fileToUpload.file.name
        const fileRef = storageRef(storage, folder + fileName)
        uploadBytes(fileRef, fileToUpload.file).catch((error) => {
            console.log(error)
        });
        return {
            folder: folder,
            fileName: fileName
        }
    };

    const onDelete = (fileToDelete: FileInfos) => {
        const fileRef = storageRef(storage, fileToDelete.folder + '/' + fileToDelete.fileName)

        deleteObject(fileRef).catch((error) => {
            console.log(error)
        });
    }

    const openFile = (file: FileInfos) => {
        console.log("openFile")
        const fileRef = storageRef(storage, file.folder + '/' + file.fileName)
        console.log(storageRef)
        getDownloadURL(fileRef).then((url) => {
            window.open(url)
        }).catch((error) => {
            console.log(error)
        });
    }

    return {
        onUpload,
        onDelete,
        openFile
    }
}