/** @type {import('tailwindcss').Config} */

module.exports = {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
    "./app.vue",
  ],
  mode: 'jit',
  plugins: [require('@tailwindcss/forms')],
  theme: {
    extend: {
      colors: {
        g: {
          light: {
            DEFAULT: '#E0E0E0',
            blue: '#7488ff',
            red: '#ff8788',
            green: '#16a34a'
          },
          dark: {
            DEFAULT: '#1F2937',
            blue: '#375BD3',
            red: '#F2545B',
            green: '#16a34a'
          }
        }
      },
      keyframes: {
        wiggle: {
          "0%, 100%": { transform: "rotate(-3deg)" },
          "50%": { transform: "rotate(3deg)" }
        }
      },
      animation: {
        wiggle: "wiggle 200ms ease-in-out"
      }
    }
  },
}