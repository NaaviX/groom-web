export const useDate = () => {

    const formatDate = (date: Date, options: Intl.DateTimeFormatOptions): string => {
        if (date) {
            return new Intl.DateTimeFormat('fr-FR', options).format(new Date(date))
        }
        return ""
    }    

    const formatShortDate = (date: Date): string => {
        return formatDate(date, { dateStyle: "short" })
    }

    const formatMediumDate = (date: Date): string => {
        return formatDate(date, { dateStyle: "medium" })
    }

    const formatLongDate = (date: Date): string => {
        return formatDate(date, { dateStyle: "long" })
    }

    const formatFullDate = (date: Date): string => {
        return formatDate(date, { dateStyle: "full" })
    }

    const formatInputDate = (date: Date): string => {
        if (date) {
            return new Intl.DateTimeFormat("fr-CA", {year: "numeric", month: "2-digit", day: "2-digit"}).format(new Date(date))
        }
        return ""
    }

    return { formatInputDate, formatShortDate, formatMediumDate, formatLongDate, formatFullDate }

}