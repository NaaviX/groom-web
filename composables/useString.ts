export const useString = () => {

    const formatPhone = (phoneNumber: string): string => {
        if (phoneNumber) {
            const SPACE = " "
            return phoneNumber.substring(0, 2) + SPACE
                + phoneNumber.substring(2, 4) + SPACE
                + phoneNumber.substring(4, 6) + SPACE
                + phoneNumber.substring(6, 8) + SPACE
                + phoneNumber.substring(8, 10);
        }
        return ""
    }

    const formatPrice = (price: number) => {
        return price + "\u00A0€"
    }

    return { formatPhone, formatPrice }

}