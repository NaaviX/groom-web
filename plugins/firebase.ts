import { initializeApp } from "firebase/app";
import {
    getAuth
} from "firebase/auth";

export default defineNuxtPlugin(async nuxtApp => {
    // Your web app's Firebase configuration
    const firebaseConfig = {

        apiKey: "AIzaSyBWL6QGvL6SwZB4L-y5Kqd_Z28D42Bd0Is",
      
        authDomain: "groomportfolio-5c37d.firebaseapp.com",
      
        projectId: "groomportfolio-5c37d",
      
        storageBucket: "groomportfolio-5c37d.appspot.com",
      
        messagingSenderId: "1042503125103",
      
        appId: "1:1042503125103:web:fe74447e61cacb7d8c659a"
      
      };
      
      

    

    // Initialize Firebase
    const app = initializeApp(firebaseConfig);

    await initUser();

    const auth = getAuth(app);

    nuxtApp.vueApp.provide('app', app);
    nuxtApp.provide('app', app);

    nuxtApp.vueApp.provide('auth', auth);
    nuxtApp.provide('auth', auth);
})