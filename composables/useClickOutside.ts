export const useClickOutside = (targetRef, callback_fn) => {
    if(!targetRef) return

    let firstClick = 0

    let listener = (e) => {
        if(firstClick == 0) {
            firstClick++
            return;
        }
        if(e.target == targetRef.value || e.composedPath().includes(targetRef.value)) {
            return
        }
        if(typeof callback_fn == 'function') {
            callback_fn()
        }
    }
    
    onMounted(() =>  {
        window.addEventListener('click', listener)
    })
    onBeforeUnmount(() => {
        window.removeEventListener('click', listener)
    })

    return {
        listener
    }
}