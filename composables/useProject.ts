import { PricingChoice } from './useOption';
import { collection, doc, addDoc, query, setDoc, where, onSnapshot, deleteDoc, QuerySnapshot, DocumentSnapshot, Query } from "firebase/firestore";
import { thisProject, thisUser, useLoading } from "./useStates";
import { Step } from "./useStep";
import {
    getAuth,
} from "firebase/auth";
import { FileInfos, FileToUpload } from "./useFile";
import { Ref } from "vue"

export interface Project {
    id?: string;
    createdOn: string;
    name: string;
    company: string;
    managers: string[];
    customers: string[];
    firstName: string;
    lastName: string;
    phone: string;
    status: ProjectStatus;
    description: string;
    pricingChoices: PricingChoice[];
    steps: Step[];
}

export enum ProjectStatus {
    New = "Nouveau",
    Spec = "Spécifications",
    Dev = "Développement",
    Done = "Terminé"
}

const fieldsToValidate: Ref<Ref[]> = ref([])

export const useProject = () => {
    const { firestore } = useFirestore();

    const auth = getAuth();

    const projectsColl = collection(firestore, `projects`);

    const useStatus = (project: Project) => {
        const allStatus = Object.values(ProjectStatus)

        const isFinished = (status: ProjectStatus): boolean => {
            return allStatus.indexOf(status) < allStatus.indexOf(project.status) || useProject().isFinished(project)
        }

        const isOngoing = (status: ProjectStatus): boolean => {
            return allStatus.indexOf(status) == allStatus.indexOf(project.status)
        }

        const isNotStarted = (status: ProjectStatus): boolean => {
            return !isFinished(status) && !isOngoing(status)
        }

        return {
            allStatus,
            isFinished,
            isOngoing,
            isNotStarted
        }
    }


    const onSubmit = () => {
        useLoading().value = true
        const project = thisProject().value!
        if (project.id) {
            const projectRef = doc(firestore, `projects`, project.id)
            const { onDelete, onUpload } = useFile()
            filesToDelete().value.forEach((fileToDelete: FileInfos) => {
                onDelete(fileToDelete)
            })
            filesToDelete().value = []

            filesToUpload().value.forEach((fileToUpload: FileToUpload) => {
                project.steps.find(step => step.ref === fileToUpload.object.ref)?.files.push(onUpload(fileToUpload)!)
            })
            filesToUpload().value = []
            setDoc(projectRef, project).then((snapshot) => {

                useLoading().value = false
            }).catch((error) => {
                useLoading().value = false
                console.log(error)
            });

        } else {
            addDoc(projectsColl, project).
                then((snapshot) => {
                    thisProject().value!.id = snapshot.id
                    navigateTo('/project/' + snapshot.id)
                    useLoading().value = false
                })
                .catch((error) => {
                    useLoading().value = false
                    console.log(error)

                });
        }
    }

    const onFetchOne = (id: string) => {
        useLoading().value = true
        const projectRef = doc(firestore, `projects`, id)
        onSnapshot(projectRef, (doc: DocumentSnapshot) => {
            thisProject().value = {
                id: doc.id, ...(doc.data())
            } as Project
            useLoading().value = false
        });
    }

    const getQuery = (): Query => {
        const roles = thisUser().value!.roles!
        if (roles.includes('manager')) {
            return query(projectsColl)
        } else {
            return query(projectsColl, where("customers", "array-contains", auth.currentUser!.email))
        }
    }

    const onFetch = () => {
        if (thisUser().value) {
            useLoading().value = true
            onSnapshot(getQuery(), (querySnapshot: QuerySnapshot) => {
                allProjects().value = []
                querySnapshot.forEach((doc) => {
                    const project = {
                        id: doc.id, ...(doc.data())
                    } as Project
                    allProjects().value.push(project)
                })
                sortProjects()
                useLoading().value = false
            });
        }
    }

    const sortProjects = () => {
        const projects = allProjects().value
        return projects.sort((a: Project, b: Project) => {
            return Object.values(ProjectStatus).indexOf(a.status) - Object.values(ProjectStatus).indexOf(b.status)
        })
    }

    const onDelete = () => {
        if (confirm("Supprimer le projet " + thisProject().value?.name + " ?")) {
            const projectRef = doc(firestore, `projects`, thisProject().value!.id!)
            deleteDoc(projectRef).then(() => {
                onClose()
            }).catch((error) => {
                console.log(error)
            })
        }
    }

    const onOpen = (project: Project) => {
        thisProject().value = project
    }

    const onClose = () => {
        navigateTo('/')
        thisProject().value = null
    }

    const getFullName = (project: Project): string => {
        return project.firstName + " " + project.lastName
    }

    const isNew = (project: Project): boolean => {
        return !project.id
    }

    const isFinished = (project: Project): boolean => {
        return Object.values(ProjectStatus).indexOf(project.status) + 1 == Object.values(ProjectStatus).length
    }

    const getNew = (): Project => {
        const { onInit } = useStep()
        const { formatInputDate } = useDate()
        return {
            createdOn: formatInputDate(new Date()),
            name: "",
            company: "",
            managers: [auth.currentUser?.email],
            customers: [""],
            firstName: "",
            lastName: "",
            phone: "",
            status: ProjectStatus.New,
            description: "",
            pricingChoices: [],
            steps: [onInit()],
        } as Project
    }

    return {
        onSubmit,
        onDelete,
        onFetchOne,
        onFetch,
        getFullName,
        getNew,
        onOpen,
        onClose,
        isNew,
        isFinished,
        useStatus,
        fieldsToValidate
    }

}
