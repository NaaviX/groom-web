
import { Project } from "./useProject";
import { Step } from "./useStep";
import { GroomUser } from "./useAuth";
import { FileInfos, FileToUpload } from "./useFile";
import { Viewer } from "@pdfme/ui";

export const thisUser = () => useState<GroomUser | null>('thisUser', () => null)
export const allProjects = () => useState<Project[]>('allProjects', () => [])
export const thisProject = () => useState<Project | null>('thisProject', () => null)
export const filesToUpload = () => useState<FileToUpload[]>('filesToUpload', () => [])
export const filesToDelete = () => useState<FileInfos[]>('filesToDelete', () => [])
export const thisStep = () => useState<Step | null>('thisStep', () => null)
export const useModalQRCode = () => useState<boolean>('openModal', () => false)
export const thisSaleOrder = () => useState<Viewer | null>('thisSaleOrder', () => null)
export const usePdfViewer = () => useState<Viewer | null>('usePdfViewer', () => null)
export const useLoading = () => useState<boolean>('useLoading', () => false)
