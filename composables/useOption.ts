import { thisProject } from './useStates';
export interface PricingCard {
    id: number;
    type: string;
    amount: string;
    amountInfos: PricingAmountInfo;
    description: string;
    including: string[];
}

export interface PricingCardSection {
    pricingSection: PricingSection;
    pricingCardList: PricingCard[];
}

export interface PricingChoice {
    pricingSection: PricingSection;
    pricingCard: PricingCard;
}

export enum PricingSection {
    Size = "Taille du site",
    Services = "Services",
    Maintenance = "Maintenance",
    Graphics = "Charte graphique",
    Custom = "Personnalisé"
}

export enum PricingAmountInfo {
    EMPTY = "",
    TTC = "(TTC)",
    Yearly = "par an (TTC)",
    Monthly = "par mois (TTC)"
}

export const useOption = () => {
    const project = thisProject().value!

    const {
        onUpdate
    } = useSaleOrder()

    const getSectionId = (pricingSection: PricingSection): string => {
        return pricingSection.replaceAll(' ', '')
    }

    const isSelected = (pricingChoice: PricingChoice): boolean => {
        return project.pricingChoices.map((pc) => pc.pricingCard.id).includes(pricingChoice.pricingCard.id)
    }

    const onSelect = (pricingChoice: PricingChoice) => {
        if (isSelected(pricingChoice)) {
            project.pricingChoices.splice(project.pricingChoices.map((pc) => pc.pricingCard.id).indexOf(pricingChoice.pricingCard.id), 1);
            onUpdate()
            return;
        }
        project.pricingChoices?.forEach((pc, index) => {
            if (pc.pricingSection === pricingChoice.pricingSection) {
                project.pricingChoices.splice(index, 1);
            }
        });
        if (!project.pricingChoices) {
            project.pricingChoices = []
        }
        navigateTo('#' + getSectionId(pricingChoice.pricingSection))
        project.pricingChoices.push(pricingChoice)
        onUpdate()
    }

    const getTotalPrice = (): number => {
        return project.pricingChoices && project.pricingChoices.length !== 0 ? project.pricingChoices.map((pc) => +pc.pricingCard.amount).reduce((amount1, amount2) => amount1 + amount2) : 0
    }

    const getTotalPricePerMonth = (): number => {
        const optionsPerMonth = project.pricingChoices.filter((pc) => pc.pricingCard.amountInfos === PricingAmountInfo.Monthly)
        return optionsPerMonth.length !== 0 ? project.pricingChoices && optionsPerMonth.map((pc) => +pc.pricingCard.amount).reduce((amount1, amount2) => amount1 + amount2) : 0
    }

    const getTotalPricePerYear = (): number => {
        const optionsPerYear = project.pricingChoices.filter((pc) => pc.pricingCard.amountInfos === PricingAmountInfo.Yearly)
        return optionsPerYear.length !== 0 ? project.pricingChoices && optionsPerYear.map((pc) => +pc.pricingCard.amount).reduce((amount1, amount2) => amount1 + amount2) : 0
    }

    const onCreate = (): PricingCard => {
        return {
            id: 100 + thisProject().value!.pricingChoices.length,
            type: "",
            amount: "",
            amountInfos: PricingAmountInfo.EMPTY,
            description: "",
            including: [""]
        }
    }

    const onAdd = (pricingCard: PricingCard) => {
        project.pricingChoices.push({
            pricingSection: PricingSection.Custom,
            pricingCard: pricingCard
        })
        onUpdate()
    }

    const onRemove = (pricingChoice: PricingChoice) => {
        if (confirm("Supprimer ce service ?")) {
            project.pricingChoices.splice(project.pricingChoices.indexOf(pricingChoice))
        }
        onUpdate()
    }

    return {
        isSelected, onSelect, onCreate, onAdd, onRemove, getSectionId, getTotalPrice, getTotalPricePerMonth, getTotalPricePerYear
    }
}

export const getPricingCardSectionList = (): PricingCardSection[] => {

    const sizeCard1: PricingCard = {
        id: 1,
        type: "Site vitrine",
        amount: "1800",
        amountInfos: PricingAmountInfo.TTC,
        description: "Description",
        including: ["3 fonctionnalités :", "Multilingue", "Google Analytic", "Formulaire", "Contenu site"]
    }
    const sizeSection: PricingCardSection = {
        pricingSection: PricingSection.Size,
        pricingCardList: [sizeCard1]
    }

    const serviceCard1: PricingCard = {
        id: 2,
        type: "Standard",
        amount: "200",
        amountInfos: PricingAmountInfo.Yearly,
        description: "Offre comprise avec le service Maintenance",
        including: ["Accès suivi client", "Mode d'utilisation", "Forfait révision: 3 par an"]
    }
    const serviceCard2: PricingCard = {
        id: 3,
        type: "Premium",
        amount: "500",
        amountInfos: PricingAmountInfo.Yearly,
        description: "Description",
        including: ["Accès suivi client", "Mode d'utilisation", "Mail pro", "Délais révision de texte ou éléments : 2 semaines", "Forfait révision: 7 par an"]
    }
    const serviceCard3: PricingCard = {
        id: 4,
        type: "VIP",
        amount: "900",
        amountInfos: PricingAmountInfo.Yearly,
        description: "Offre comprise avec le service Maintenance",
        including: ["Accès suivi client", "Mode d'utilisation", "Mail pro", "Délais révision de texte ou éléments : 1 semaine", "Forfait révision: 12 par an"]
    }
    const serviceSection: PricingCardSection = {
        pricingSection: PricingSection.Services,
        pricingCardList: [serviceCard1, serviceCard2, serviceCard3]
    }

    const maintenanceCard1: PricingCard = {
        id: 5,
        type: "Standard",
        amount: "500",
        amountInfos: PricingAmountInfo.Yearly,
        description: "Services standard compris dans l'offre",
        including: ["Mises à jour toute l'année", "Mise en ligne du site", "Gestion nom de domaine", "Maintenance opérationnelle", "", "Gamme utilisateur limitée à 1000 utilisateurs/jours"]
    }
    const maintenanceCard2: PricingCard = {
        id: 6,
        type: "Standard",
        amount: "+500",
        amountInfos: PricingAmountInfo.Yearly,
        description: "Description",
        including: ["Gamme supérieur à 1000 utilisateurs/jours", "Devis Mensuel"]
    }
    const maintenanceSection: PricingCardSection = {
        pricingSection: PricingSection.Maintenance,
        pricingCardList: [maintenanceCard1, maintenanceCard2]
    }

    const graphicCard1: PricingCard = {
        id: 7,
        type: "Charte graphique",
        amount: "800",
        amountInfos: PricingAmountInfo.TTC,
        description: "Description",
        including: ["Charte graphique", "Logo", "Identité graphique", "Photos/images"]
    }
    const graphicSection: PricingCardSection = {
        pricingSection: PricingSection.Graphics,
        pricingCardList: [graphicCard1]
    }

    const customSection: PricingCardSection = {
        pricingSection: PricingSection.Custom,
        pricingCardList: []
    }

    return [sizeSection, serviceSection, maintenanceSection, graphicSection, customSection]
}