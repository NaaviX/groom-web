import { FileInfos, FileToUpload } from "./useFile";

export const useFileUpload = () => {
    const dragActive = ref(false);

    const upload = (fileList: FileList) => {
        const step = thisStep().value!
        const project = thisProject().value!
        for (const file of fileList) {
            const folder = 'pj/' + project.id + '/' + step.ref
            filesToUpload().value.push({
                file: file,
                folder: folder,
                object: step
            } as FileToUpload)
        }
    }

    const onDragActive = () => {
        dragActive.value = !dragActive.value;
    };

    const onDrop = (event: DragEvent) => {
        upload(event.dataTransfer!.files)
        dragActive.value = false
    };

    const onSelectFile = (event: Event) => {
        const target = event.target as HTMLInputElement
        upload(target.files!)
    };

    const onDeleteFileToUpload = (fileToUpload: FileToUpload) => {
        filesToUpload().value.splice(filesToUpload().value.indexOf(fileToUpload), 1)
    };

    const onDeleteFile = (fileInfos: FileInfos) => {
        const step = thisStep().value!
        filesToDelete().value.push(step.files.splice(step.files.indexOf(fileInfos), 1).at(0)!)
    };

    const newFiles = computed(() => {
        return (filesToUpload().value as FileToUpload[])?.filter((file) => {
            return file.object.ref === thisStep().value!.ref
        })
    })

    return {
        dragActive,
        onDragActive,
        onDrop,
        onSelectFile,
        onDeleteFileToUpload,
        onDeleteFile,
        newFiles
    }
}
