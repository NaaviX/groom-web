import { Viewer } from '@pdfme/ui';
import {
    User,
    getAuth,
    signInWithEmailAndPassword,
    onAuthStateChanged,
} from "firebase/auth";
import { thisUser } from "./useStates";
import { doc, getDoc, setDoc } from "firebase/firestore";


export interface GroomUser {
    auth: User | null;
    roles: string[] | null;
}

const errorMessages: { [key: string]: string } = {
    "auth/user-not-found": "Identifiant invalide. Veuillez vérifier votre adresse mail.",
    "auth/invalid-email": "Identifiant invalide. Veuillez vérifier votre adresse mail.",
    "auth/wrong-password": "Mot de passe incorrect. Veuillez vérifier votre mot de passe et réessayer.",
    "auth/network-request-failed": "Impossible de se connecter. Vérifiez votre connexion Internet et réessayez plus tard.",
    "auth/internal-error": "Le service auquel vous tentez de vous connecter n'est pas disponible pour le moment. Veuillez réessayer plus tard.",
    "auth/too-many-requests": "Vous avez dépassé la limite de tentatives de connexion. Votre compte est temporairement verrouillé. Veuillez réessayer après quelques minutes."
};

export const signInUser = async (email: string, password: string) => {
    const auth = getAuth();
    await signInWithEmailAndPassword(auth, email, password)
        .catch((error) => {
            const errorCode: string = error.code;
            console.log(errorCode)
            throw new Error(errorMessages[errorCode] || "Une erreur c'est produite.")
        });
}

export const signOutUser = async () => {
    const auth = getAuth();
    const error = await auth.signOut()
    thisUser().value = null
    return error
}

export const initUser = async () => {
    const { firestore } = useFirestore();
    const auth = getAuth();



    await onAuthStateChanged(auth, async () => {
        const user = auth.currentUser
        if (user !== null) {
            const userDoc = doc(firestore, 'users', user.uid);
            useLoading().value = true
            await getDoc(userDoc).then(async (snapshot) => {
                console.log(snapshot)
                if (!snapshot.data()) {
                    await setDoc(userDoc, {
                        displayName: user.displayName,
                        photoURL: user.photoURL,
                        email: user.email,
                        roles: []
                    }).then(() => {
                        thisUser().value = {
                            auth: user,
                            roles: []
                        }
                    })
                } else {
                    thisUser().value = {
                        auth: user,
                        roles: snapshot.data()!.roles
                    }
                }
            })
            useLoading().value = false
        }

    });
}

export const isManager = (): boolean => {
    return thisUser().value!.roles!.includes('manager')
}