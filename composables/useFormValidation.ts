import { Ref } from 'vue';

const hasError: Ref<Boolean> = ref(false)

export const useFormValidation = () => {

    const error = ref("")

    const validateField = (fieldValue: string) => {
        error.value = ""
        if(!fieldValue) {
            updateErrors("Veuillez remplir ce champs")
        }
    }

    const validatePhoneField = (fieldValue: string) => {
        validateField(fieldValue)
        
        if(!error.value) {
            const re = /^\d{10}$/;
            if(!re.test(fieldValue)) {
                updateErrors("Numéro de téléphone non valide")
            }
        }
    }

    const validateEmailField = (fieldValue: string) => {
        validateField(fieldValue)
        
        if(!error.value) {
            const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if(!re.test(fieldValue)) {
            updateErrors("Adresse email non valide")
            }
        }
    }

    const updateErrors = (e: string) => {
        hasError.value = true
        error.value = e
    }

    const validateForm = (...inputs: Ref[]) => {
        hasError.value = false
        inputs.forEach((input) => {
            input.value.onValidate()
        })
    }

    return { error, hasError, validateField, validatePhoneField, validateEmailField, validateForm }

}