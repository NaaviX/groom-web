import { getFirestore } from "firebase/firestore";

export const useFirestore = () => {

    const { firebaseApp } = useNuxtApp();

    const firestore = getFirestore(firebaseApp);

    return {
        firestore
    }
}