import { Template, Viewer } from '@pdfme/ui';
import { generate } from '@pdfme/generator';
import saleOrderTemplate from '~~/assets/static/templates/sale_order.json'
import { PricingChoice } from './useOption';
import { Project } from './useProject';
import { addDoc, collection, doc, getCountFromServer, setDoc } from 'firebase/firestore';

export interface SaleOrder {
    saleOrderRef: string;
    saleOrderDate: string;
    customerName: string;
    companyName: string;
    email: string;
    phone: string;
    service1Libelle: string;
    service2Libelle: string;
    service3Libelle: string;
    service4Libelle: string;
    service5Libelle: string;
    service1Name: string;
    service2Name: string;
    service3Name: string;
    service4Name: string;
    service5Name: string;
    service1Qty: string;
    service2Qty: string;
    service3Qty: string;
    service4Qty: string;
    service5Qty: string;
    service1Price: string;
    service2Price: string;
    service3Price: string;
    service4Price: string;
    service5Price: string;
    totalPrice: string;
}

const projectToSaleOrderConverter = async (): Promise<SaleOrder> => {
    const project = thisProject().value!
    const services = project.pricingChoices as PricingChoice[]
    const { formatLongDate } = useDate()
    const { getFullName } = useProject()
    const { getTotalPrice } = useOption()

    const saleOrderRef = await getOrderReference()

    return {
        saleOrderRef: saleOrderRef?.toString(),
        saleOrderDate: formatLongDate(new Date()),
        customerName: getFullName(project),
        companyName: project.company,
        email: project.customers[0],
        phone: project.phone,
        service1Libelle: services.at(0)?.pricingSection ?? "",
        service2Libelle: services.at(1)?.pricingSection ?? "",
        service3Libelle: services.at(2)?.pricingSection ?? "",
        service4Libelle: services.at(3)?.pricingSection ?? "",
        service5Libelle: services.at(4)?.pricingSection ?? "",
        service1Name: services.at(0)?.pricingCard.type ?? "",
        service3Name: services.at(2)?.pricingCard.type ?? "",
        service2Name: services.at(1)?.pricingCard.type ?? "",
        service4Name: services.at(3)?.pricingCard.type ?? "",
        service5Name: services.at(4)?.pricingCard.type ?? "",
        service1Qty: services.length > 0 ? "1" : "",
        service2Qty: services.length > 1 ? "1" : "",
        service3Qty: services.length > 2 ? "1" : "",
        service4Qty: services.length > 3 ? "1" : "",
        service5Qty: services.length > 4 ? "1" : "",
        service1Price: formatPrice(services.at(0)?.pricingCard.amount ?? null),
        service2Price: formatPrice(services.at(1)?.pricingCard.amount ?? null),
        service3Price: formatPrice(services.at(2)?.pricingCard.amount ?? null),
        service4Price: formatPrice(services.at(3)?.pricingCard.amount ?? null),
        service5Price: formatPrice(services.at(4)?.pricingCard.amount ?? null),
        totalPrice: formatPrice(getTotalPrice().toString())
    }
}

export const useSaleOrder = () => {

    const { firestore } = useFirestore();

    const template: Template = JSON.parse(JSON.stringify(saleOrderTemplate)) as Template;

    const saleOrderColl = collection(firestore, `devis`);

    const onCreate = (domContainer: HTMLElement) => {
        const inputs = [{}]
        usePdfViewer().value = new Viewer({ domContainer, template, inputs });
    }

    const onUpdate = async () => {
        const saleOrder = await projectToSaleOrderConverter() as SaleOrder
        saleOrder.saleOrderRef = (await getLastOrderID() + 1).toString()
        const inputs = [
            JSON.parse(JSON.stringify(saleOrder))
        ]
        usePdfViewer().value?.setInputs(inputs)
    }

    const onGenerate = async () => {
        const saleOrder = await projectToSaleOrderConverter() as SaleOrder
        await submit(saleOrder)
        const inputs = [
            JSON.parse(JSON.stringify(saleOrder))
        ]
        usePdfViewer().value?.setInputs(inputs)
        generate({ template, inputs }).then((pdf: Uint8Array) => {
            const blob = new Blob([pdf.buffer], { type: 'application/pdf' });
            window.open(URL.createObjectURL(blob));
        })
    }

    const submit = async (saleOrder: SaleOrder) => {
        try {
            await setDoc(doc(firestore, `devis/${await getOrderReference()}`), saleOrder);
        } catch (error) {
            console.log(error);
        }
    }

    return {
        onCreate,
        onUpdate,
        onGenerate
    }

}

const getLastOrderID = async (): Promise<number> => {
    const { firestore } = useFirestore();
    const collectionRef = collection(firestore, `devis`);
    const snapshot = await getCountFromServer(collectionRef);
    return snapshot.data().count
}

const getOrderReference = async () => (await getLastOrderID() + 1).toString()

const formatPrice = (price: string | null): string => {
    return price ? price + " EUR" : ""
}