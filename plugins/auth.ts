export default defineNuxtPlugin(() => {
    addRouteMiddleware('authenticated', async () => {
        const { $auth } = useNuxtApp()
        await $auth?.operations
        if (!$auth?.currentUser?.uid) {
            return navigateTo('/login', { redirectCode: 401 })
        }
    })
    addRouteMiddleware('unauthenticated', async () => {
        const { $auth } = useNuxtApp()
        await $auth?.operations
        if ($auth?.currentUser?.uid) {
            return navigateTo('/', { redirectCode: 401 })
        }
    })
})
