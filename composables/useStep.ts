import { thisProject } from "./useStates";
import { FileInfos, FileToUpload } from "./useFile";
import { thisStep } from "./useStates";
import { sortUserPlugins } from "vite";

export interface Step {
    ref: number;
    name: string;
    startDate: string;
    lastUpdatedDate: string;
    endDate: string;
    infos: string[];
    description: string;
    link: string;
    files: FileInfos[];
}

export const useStep = () => {
    const project = thisProject().value!
    const { formatInputDate } = useDate()

    const useFile = () => {
        const onDeleteUpload = (fileToUpload: FileToUpload) => {
            filesToUpload().value.splice(filesToUpload().value.indexOf(fileToUpload), 1)
        };

        const onDelete = (step: Step, fileInfos: FileInfos) => {
            filesToDelete().value.push(step.files.splice(step.files.indexOf(fileInfos), 1).at(0)!)
        };

        return {
            onDeleteUpload,
            onDelete
        }
    }

    const onOpen = (step: Step) => {
        thisStep().value = step
    }

    const onClose = () => {
        thisStep().value = null
    }

    const onCreate = (): Step => {
        const step = {
            ref: project.steps.length + 1,
            name: "",
            startDate: formatInputDate(new Date()),
            endDate: "",
            description: "",
            infos: [],
            link: "",
            files: [],
            lastUpdatedDate: ""
        } as Step
        thisStep().value = step
        return step
    }

    const onSave = (s?: Step) => {
        const step = s ? s : thisStep().value!

        if (isNew(step)) {
            project.steps.push(step)
            sort()
        }
        onClose()
    }

    const onInit = (): Step => {
        const step = {
            ref: 1,
            name: "Initiation",
            startDate: formatInputDate(new Date()),
            endDate: formatInputDate(new Date()),
            description: "Création du projet",
            infos: [],
            link: "",
            files: [],
            lastUpdatedDate: ""
        } as Step
        return step
    }

    const onStart = (s?: Step) => {
        const step = s ? s : thisStep().value!
        step.startDate = formatInputDate(new Date())
    }

    const onFinish = (s?: Step) => {
        const step = s ? s : thisStep().value!
        step.endDate = formatInputDate(new Date())
    }

    const onDelete = (s?: Step) => {
        const step = s ? s : thisStep().value!
        if (confirm("Supprimer l'étape " + step.name + " ?")) {
            const { onDelete, onDeleteUpload } = useFile()

            project.steps.splice(project.steps.map((s) => s.ref).indexOf(step.ref), 1)
            step.files.forEach((file: FileInfos) => {
                onDelete(step, file)
            })
            filesToUpload().value.forEach((fileToUpload: FileToUpload) => {
                if (fileToUpload.object.ref === step.ref) {
                    onDeleteUpload(fileToUpload)
                }
            })
            onClose()
        }
    }

    const sort = () => {
        project.steps?.sort((s1, s2) => s2.ref - s1.ref)
    }

    const isNew = (s?: Step): boolean => {
        const step = s ? s : thisStep().value!
        const projectSteps = project.steps
        return !project || !projectSteps || !projectSteps.includes(step)
    }

    const isOngoing = (s?: Step): boolean => {
        const step = s ? s : thisStep().value!
        return step.startDate <= formatInputDate(new Date()) && !isFinished(step)
    }

    const isFinished = (s?: Step): boolean => {
        const step = s ? s : thisStep().value!
        return step.endDate !== '' && step.endDate <= formatInputDate(new Date())
    }

    const isNotStarted = (s?: Step): boolean => {
        const step = s ? s : thisStep().value!
        return step.startDate > formatInputDate(new Date())
    }

    return { onOpen, onClose, onCreate, onSave, onInit, onStart, onFinish, onDelete, sort, isNew, isOngoing, isFinished, isNotStarted }
}